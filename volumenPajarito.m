
% %%%%%%%%%%%%%%%%%%%%%%%
%
% Represento la situación gráficamente
%
% %%%

% Para simplificar los cálculos se asumen las siguientes posiciones de las
% circunferencias

% Estas variables son sólo para representar gráficamente le problema. Las
% funciones de cálculo asumen los mismos valores.

circAXcentro = -0.5;
circAYcentro = 0.0;
circAZcentro = 0.0;

circAradio = 1;

circBXcentro = 0.5;
circBYcentro = 0.0;
circBZcentro = 0.0;

circBradio = 1;

cantidadPuntos = 10000;

phi = 2*pi * rand(1, cantidadPuntos);
theta = 2*pi * rand(1, cantidadPuntos);

puntosAX = circAradio .* cos(theta) .* cos(phi) + circAXcentro;
puntosAY = circAradio .* cos(theta) .* sin(phi) + circAYcentro;
puntosAZ = circAradio .* sin(theta) + circAYcentro;

puntosBX = circBradio .* cos(theta) .* cos(phi) + circBXcentro;
puntosBY = circBradio .* cos(theta) .* sin(phi) + circBYcentro;
puntosBZ = circBradio .* sin(theta) + circBZcentro;

% encuentro los puntos que pertenecen a la superficie S que delimita el
% volumen

figure
plot3(puntosAX, puntosAY, puntosAZ, 'r.', puntosBX, puntosBY, puntosBZ, 'b.', circAXcentro, circAYcentro, circAZcentro, 'r*', circBXcentro, circBYcentro, circBZcentro, 'b*');
text(circAXcentro + 0.1, circAYcentro, 'A');
text(circBXcentro + 0.1, circBYcentro, 'B');
axis equal;
xlim([-1.5 1.5]);
ylim([-1.5 1.5]);
zlim([-1.5 1.5]);
grid on;

indexAenB = (circBradio > sqrt((puntosAX - circBXcentro).^2 + (puntosAY - circBYcentro).^2 + (puntosAZ - circBZcentro).^2));
indexBenA = (circAradio > sqrt((puntosBX - circAXcentro).^2 + (puntosBY - circAYcentro).^2 + (puntosBZ - circAZcentro).^2));

puntosSX = [puntosAX(indexAenB) puntosBX(indexBenA)];
puntosSY = [puntosAY(indexAenB) puntosBY(indexBenA)];
puntosSZ = [puntosAZ(indexAenB) puntosBZ(indexBenA)];

figure
plot3(puntosSX, puntosSY, puntosSZ, 'r.', circAXcentro, circAYcentro, circAZcentro, 'r*', circBXcentro, circBYcentro, circBZcentro, 'b*');
text(circAXcentro - 0.2, circAYcentro, 'A');
text(circBXcentro + 0.2, circBYcentro, 'B');
axis equal;
xlim([-1.5 1.5]);
ylim([-1.5 1.5]);
zlim([-1.5 1.5]);
grid on;

%

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Cálculo de la superficie
%
% %%%

% Establezco con cuántos dígitos de precisión quiero el resultado
digitosPrecision = 2;

% Le doy un valor inicial al paso de integración utilizado para discretizar
% el eje X para integrar numéricamente.
pasoIntegracionAnterior = 0.1;

% Calculo el área para ese paso. Necesito este valor como referencia
% inicial antes de empezar a iterar.
resultadoVolumenAnterior = funcionPajarito(pasoIntegracionAnterior);

% Ahora comienzo a iterar, incrementando 10 veces la cantidad de puntos
% considerados durante la integración en cada paso. 
%
% Al condición de parada es que la diferencia del nuevo valor de área 
% calculado tenga una diferencia con el valor calculado en la iteración
% anterior que sea despreciable.
%
% La definición de "despreciable" en este contexto es: "10 veces menor que 
% la resolución deseada en el resultado" (dada por los dígitos de 
% precisión).

% Ahora itero hasta alcanzar un valor paso adecuado de integración

fprintf('\n');

fprintf('Comienzo del proceso iterativo refinamiento del valor de área\n');

fprintf('\n');

listo = 0;
iteracion = 0;
while(listo == 0)
    
    iteracion = iteracion + 1;
    
    % Los pasos avanzan decalmente, aumentando la resolución de la integral
    % 10 veces cada paso.
    pasoIntegracion = pasoIntegracionAnterior / sqrt(10);
    
    % Calculo el area con el nuevo paso
    resultadoVolumen = funcionPajarito(pasoIntegracion);
    
    % Si la mejora en el valor calculado es despreciable comparado con la
    % resolución, paro.
    mejora = abs(resultadoVolumen - resultadoVolumenAnterior);
    
    % Ver definición de "despreciable" más arriba.
    if (mejora < 10^(-digitosPrecision) / 10)
        
        listo = 1;
    end
    
    fprintf(' * Iteracion %d - Paso %1.1e - Volumen calculado %f m^3 (diferencia %e)\n', iteracion, pasoIntegracion, resultadoVolumen, mejora);
    
    pasoIntegracionAnterior = pasoIntegracion;
    resultadoVolumenAnterior = resultadoVolumen;
end
fprintf('\n');

fprintf('Fin de proceso iterativo\n');

fprintf('\n');

fprintf('Valor final del volumen disponible para el pajarito: %f m^3 (luego de %d iteraciones),\n', resultadoVolumen, iteracion);
fprintf('calculado con %d dígitos de precisión\n', digitosPrecision);

fprintf('\n');




