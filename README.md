# CITES Coding Contest 2016

## Problema

Esta carpeta contiene la solución al problema "Un Perrito y un Pajarito". A partir de la consigna era necesario responder:

  1. ¿Sobre qué superficie puede moverse el perrito?
  2. ¿Sobre qué volumen puede moverse el pajarito?


## Solución

Las respuestas a las preguntas consignadas son: 

  1. El área sobre la que puede moverse el perrito es de 1.228370 m^2, calculada con 5 dígitos significativos.
  2. El volumen en el que puede mover se el pajarito es de 1.309233 m^3, calculado con 2 dígitos significativos.

Se pueden calcular las respuestas con más o menos dígitos significativos, dependiendo del tiempo de cómputo que se esté dispuesto a invertir.

## Metodología

Para ambas preguntas se procedió a resolver el problema de cálculo numéricamente mediante la utilización de sendos scripts en lenguaje Octave/Matlab. 

En los dos casos el script realiza la integral (de línea o de volumen) mediante una Suma de Rienmann de paso variable. El script automáticamente decrementa el tamaño de los intervalos de discretización en función de la cantidad de dígitos decimales sin error que se desea obtener.

El cálculo del area del perrito no presenta mayores problemas de performance, ya que la resolución numérica de la integral de línea se puede hacer muy rápidamente con cualquier computador moderno. 

El cálculo del volumen, por otro lado, tiene la dificultad de que la cantidad de celdas en que se divide la superficie a integrar crece cuadraticamente al reducir el tamaño de los intervalos de discretización de la misma. Se puede ver en mejor detalle en los comentarios en el script correspondiente, pero se puede adelantar aquí que esta característica impone un compromiso entre tiempo de ejecución y utilización de memoria al resolver el problema mediante el lenguaje interpretado de álgebra matricial de Octave/Matlab.

Para resolver esto último se parametrizó el algoritmo de integral volumétrica con un factor que permite mejorar la velocidad a costo de usar más memoria o viceversa.

## Cómo ejecutar las Soluciones

Para ejecutar la solución se debe ejecutar el script main.m en Octave o Matlab.

Dicho archivo llama a su vez a los scripts que se encargan de resolver los dos problemas individuales, y muestra los resultados en la consola de Octave/Matlab.


## Repositorio Git de la solución

Todos los archivos de la solución pueden hallarse en

https://gitlab.com/glpuga/acodearunperritoyunpajarito.git







