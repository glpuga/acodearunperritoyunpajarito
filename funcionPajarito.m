function volumenTotal = funcionPajarito(paso)


    % La primera versión del algorimo utilizaba meshgrid para calcular el
    % volumen sobre toda la proyección XY de la superficie al mismo tiempo,
    % pero eso demostró ser impractico porque consume una cantidad
    % infame de memoria cuando el paso se reduce para mejorar la precisión.
    % Eso ocurre porque las matrices utilizadas crecen en tamaño con el
    % cuadrado de la inversa del paso. Como resultado, si el paso era muy
    % chico el consumo de memoria mataba el programa.
    %
    % La segunda versión calculaba la integral de volumen una fila por vez,
    % de forma de que el tamaño de las matrices usadas crece sólo 
    % linealmente con la inversa del paso. Esta versión siempre funcionaba,
    % pero realizar tantas iteraciones hacía aumentar el tiempo de
    % ejecución rápidamente.
    %
    % Esta versión, la tercera, toma una aproximación intermedia,
    % dividiendo la superficie sobre la que se integra el volumen en
    % celdas que se iteran, pero dentra de cada una calculando el volumen
    % mediante las funciones de matrices de Matlab/Octave, logrando así muy
    % buenos tiempo con un consumo de memoria acotado.
    %
    % Esta versión se puede parametrizar para encontrar un balance entre
    % velocidad y consumo de memoria regulando el tamaño de la celda
    % matricial.
    %
    % Este parametro me permite buscar un compromiso entre tiempo de
    % ejecucion y consumo de memoria. En mi máquina 5000 da un buen
    % compromiso, pero eso depende de la cantidad de memoria disponible. Un
    % número muy chico y Matlab/Octave pierden tiempo iterando los bucles
    % for; un número muy grande y el tamaño de las matrices se vuelve tan
    % grande que el sistema operativo empieza a volcar memoria a disco y
    % los tiempos de ejecución se disparan (si es que logra ejecutarse).
    N = 5000;

    % esta funcion asume las circunferencias en la posicion representada
    % graficamente por el script, con dos circunferencias a distancia
    % unitaria y centros en (-0.5, 0.0, 0.0) y (0.5, 0.0, 0.0)
    
    % Por la simetria del problema, calculo el volumen encerrado entre los
    % planos Z=0, X=0 y la porción de la superficie que tienen coordenadas
    % de X y Z positivas y luego multiplico el resultado por cuatro.
        
    rangoX = [0:paso:0.5 - paso];
    rangoY = [-1:paso:1 - paso];
    
    % Comienzo a iterar.
    
    volumenEncerrado = 0.0;
    
    columnasPorMordida = N;
    filasPorMordida = N;
    
    for primeraColumna = [1:columnasPorMordida:length(rangoX)]
        
        for primeraFila = [1:filasPorMordida:length(rangoY)]
            
            ultimaColumna = primeraColumna + columnasPorMordida - 1;
            
            if (ultimaColumna > length(rangoX))
                ultimaColumna = length(rangoX);
            end
            
            ultimaFila = primeraFila + filasPorMordida - 1;
            
            if (ultimaFila > length(rangoY))
                ultimaFila = length(rangoY);
            end
            
            [puntosX, puntosY] = meshgrid(rangoX(primeraColumna:ultimaColumna), rangoY(primeraFila:ultimaFila));
            
            % Calculo la longitud de la proyección sobre XY del segmento que une
            % cada uno de los (X, Y, Z) al centro de la esfera.
            %
            % Notar que a) por el volumen que estoy integrando la esfera relevante
            % es la que tiene centro en (-0.5, 0, 0), y b) en este paso no necesito
            % saber todavía el valor de Z (porque quiero la medida de la proyección
            % en XY).
            distanciaCentroEnXY = sqrt((puntosX - (-0.5)).^2 + (puntosY - (0.0)).^2);
            
            % Calculo los valores de Z de la curva para estos valores de (X,Y).
            % de Z estan dados por la circunferencia que tiene centro en
            % (-0.5, 0, 0).
            puntosZ = sqrt(1 - distanciaCentroEnXY.^2);
            
            % Determino que puntos de los calculados caen fuera de la esfera (y por
            % lo tanto dieron valor de Z complejo)
            puntosZ(distanciaCentroEnXY > 1) = 0.0;
            
            % Integro el volumen de la superficie encerrada multiplicando la
            % dimensión de cada celda en el plano XY por el valor de Z en uno de
            % los puntos de la misma (aproximación rectangular).
            volumenEncerrado = volumenEncerrado + sum(sum(puntosZ * paso^2));
        end
        
    end
    
    % El volumen total es cuatro veces el encerrado en la superficie de
    % cálculo
    volumenTotal = 4 * volumenEncerrado;
end