
% %%%%%%%%%%%%%%%%%%%%%%%
%
% Represento la situación gráficamente
%
% %%%

% Para simplificar los cálculos se asumen las siguientes posiciones de las
% circunferencias

% Estas variables son sólo para representar gráficamente le problema. Las
% funciones de cálculo asumen los mismos valores.

circAXcentro = -0.5;
circAYcentro = 0.0;

circAradio = 1;

circBXcentro = 0.5;
circBYcentro = 0.0;

circBradio = 1;

angulo = [0:0.01:2*pi];

puntosAX = circAradio * cos(angulo) + circAXcentro;
puntosAY = circAradio * sin(angulo) + circAYcentro;

puntosBX = circBradio * cos(angulo) + circBXcentro;
puntosBY = circBradio * sin(angulo) + circBYcentro;

plot(puntosAX, puntosAY, 'r-', puntosBX, puntosBY, 'b-', circAXcentro, circAYcentro, 'r*', circBXcentro, circBYcentro, 'b*');
text(circAXcentro + 0.1, circAYcentro, 'A');
text(circBXcentro + 0.1, circBYcentro, 'B');
axis equal;
xlim([-1.5 1.5]);
ylim([-1.5 1.5]);
grid on;


% %%%%%%%%%%%%%%%%%%%%%%%
%
% Cálculo de la superficie
%
% %%%


% Establezco con cuántos dígitos de precisión quiero el resultado
digitosPrecision = 5;

% Le doy un valor inicial al paso de integración utilizado para discretizar
% el eje X para integrar numéricamente.
pasoIntegracionAnterior = 0.1;

% Calculo el área para ese paso. Necesito este valor como referencia
% inicial antes de empezar a iterar.
resultadoAreaAnterior = funcionPerrito(pasoIntegracionAnterior);

% Ahora comienzo a iterar, incrementando 10 veces la cantidad de puntos
% considerados durante la integración en cada paso. 
%
% Al condición de parada es que la diferencia del nuevo valor de área 
% calculado tenga una diferencia con el valor calculado en la iteración
% anterior que sea despreciable.
%
% La definición de "despreciable" en este contexto es: "10 veces menor que 
% la resolución deseada en el resultado" (dada por los dígitos de 
% precisión).

% Ahora itero hasta alcanzar un valor paso adecuado de integración

fprintf('\n');

fprintf('Comienzo del proceso iterativo refinamiento del valor de área\n');

fprintf('\n');

listo = 0;
iteracion = 0;
while(listo == 0)
    iteracion = iteracion + 1;
    
    % Los pasos avanzan decalmente, aumentando la resolución de la integral
    % 10 veces cada paso.
    pasoIntegracion = pasoIntegracionAnterior / 10;
    
    % Calculo el area con el nuevo paso
    resultadoArea = funcionPerrito(pasoIntegracion);
    
    % Si la mejora en el valor calculado es despreciable comparado con la
    % resolución, paro.
    mejora = abs(resultadoArea - resultadoAreaAnterior);
    
    % Ver definición de "despreciable" más arriba.
    if (mejora < 10^(-digitosPrecision) / 10)
        
        listo = 1;
    end
    
    fprintf(' * Iteracion %d - Paso %1.1e - Area calculada %f m^2 (diferencia %e)\n', iteracion, pasoIntegracion, resultadoArea, mejora);
    
    pasoIntegracionAnterior = pasoIntegracion;
    resultadoAreaAnterior = resultadoArea;
end
fprintf('\n');

fprintf('Fin de proceso iterativo\n');

fprintf('\n');

fprintf('Valor final del área disponible para el perrito: %f m^2 (luego de %d iteraciones),\n', resultadoArea, iteracion);
fprintf('calculado con %d dígitos de precisión\n', digitosPrecision);

fprintf('\n');




