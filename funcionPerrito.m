function area = funcionPerrito(paso)

    % esta funcion asume las circunferencias en la posicion representada
    % graficamente por el script, con dos circunferencias a distancia
    % unitaria y centros en (-0.5, 0.0) y (0.5, 0.0)
    
    % Por la simetria del problema, calculo el area den el primer cuadrante
    % y multiplico el resultado por cuatro
    
    puntosX = [0:paso:0.5];
    
    % Calculo los valores de la curva para estos valores de X. Por estar
    % lidiando solamente con la mitad derecha de la superficie, los valores
    % de Y estan dados por la circunferencia que tiene centro en -0.5.
    
    puntosY = sqrt(1 - (puntosX - (-0.5)).^2);
    
    % Calculo la superficie por trapecios
    mediaArea = sum((puntosY(1:end-1) + puntosY(2:end)) * paso / 2);
    
    % El area total es del cuadruple
    area = 4 * mediaArea;
end